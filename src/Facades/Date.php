<?php

namespace Titan\Facades;

use Titan\Kernel\Facade;

class Date extends Facade
{
    /**
     * Get the registered name of the component
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'date';
    }
}