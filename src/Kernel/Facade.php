<?php

namespace Titan\Kernel;

abstract class Facade
{
    /**
     * Application list in Service Provider array
     *
     * @var array
     */
    protected static $applications;

    /**
     * Resolved instances of objects in Facade array
     *
     * @var array
     */
    protected static $resolvedInstances = [];

    /**
     * Created instances of objects in Facade array
     *
     * @var array
     */
    protected static $createdInstances  = [];

    /**
     * Set facade application container
     *
     * @param $app
     * @return void
     */
    public static function setFacadeApplication($app)
    {
        static::$applications = $app;
    }

    /**
     * Get facade application container
     *
     * @return array
     */
    public static function getFacadeApplication()
    {
        return static::$applications;
    }

    /**
     * Clear resolved instances
     *
     * @param string $facadeName
     * @return void
     */
    public static function clearResolvedInstance(string $facadeName)
    {
        unset(static::$resolvedInstances[$facadeName]);
    }

    /**
     * Resolve instance
     *
     * @param string $facadeName
     * @return string
     */
    protected static function resolveInstance(string $facadeName)
    {
        if (is_object($facadeName)) {
            return $facadeName;
        }

        if (isset(static::$resolvedInstances[$facadeName])) {
            return static::$resolvedInstances[$facadeName];
        }

        return static::$resolvedInstances[$facadeName] = static::$applications->resolve($facadeName);
    }

    /**
     * Call methods in Application Object
     *
     * @param string $method
     * @param array $args
     * @return mixed
     */
    public static function __callStatic(string $method, array $args = [])
    {
        $accessor = strtolower(static::getFacadeAccessor());
        $provider = static::resolveInstance($accessor);

        if (!array_key_exists($accessor, static::$createdInstances)) {
            static::$createdInstances[$accessor] = $provider;
        }

        return call_user_func_array([static::$createdInstances[$accessor], $method], $args);
    }

}
