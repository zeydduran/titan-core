<?php

namespace Titan\Libraries\Database;

use Titan\Container\Container;
use Opis\Database\Database;
use Opis\Database\Connection;
use PDO;

class DB
{
    /**
     * @var Database
     */
    private $db;

    /**
     * DB constructor.
     *
     * @param Container $container
     * @throws \ReflectionException
     */
    public function __construct(Container $container)
    {
        // Connection parameters
        $config = $container->resolve('config')->load('database')->get();

        // Connection string
        $dsn    = '';

        // Setting connection string
        if (in_array($config['db_driver'], ['mysql', 'pgsql', 'mssql', 'sybase', 'dblib'])) {
            $dsn = $config['db_driver'] . ':host=' . $config['db_host'] . ';dbname=' . $config['db_name'];
        } else if ($config['db_driver'] == 'sqlite') {
            $dsn = 'sqlite:' . $config['db_name'];
        } else if ($config['db_driver'] == 'oracle' || $config['db_driver'] == 'oci') {
            $dsn = 'oci:dbname=' . $config['db_host'] . '/' . $config['db_name'];
        } else if ($config['db_driver'] == 'sqlsrv') {
            $dsn = 'sqlsrv:server=' . $config['db_host'] . ';database=' . $config['db_name'];
        }

        // Creating a connection
        $connection = new Connection($dsn, $config['db_user'], $config['db_pass']);

        // Setting connection options
        $connection->option(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ)
                   ->option(PDO::ATTR_STRINGIFY_FETCHES, false)
                   ->initCommand("SET NAMES '" . $config['db_charset'] . "' COLLATE '" . $config['db_collation'] . "'")
                   ->initCommand("SET CHARACTER SET '" . $config['db_charset'] . "'");

        // Making the connection persistent
        if ($config['db_persistent'] === true) {
            $connection->persistent();
        }

        $this->db = new Database($connection);
    }

    /**
     * Call Database method
     *
     * @param string $method
     * @param $args
     * @return mixed
     */
    public function __call(string $method, array $args = [])
    {
        return call_user_func_array(
            [$this->db, $method],
            $args
        );
    }
}