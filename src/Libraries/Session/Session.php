<?php

namespace Titan\Libraries\Session;

use Titan\Libraries\Config\Config;

class Session
{
    /**
     * Config parameters for session.
     *
     * @var mixed
     */
    private $config;

    /**
     * Session constructor.
     *
     * @param Config $config
     * @throws \ReflectionException
     * @throws \Titan\Exception\ExceptionHandler
     */
    public function __construct(Config $config)
    {
        $this->config = $config->load('app')->get('session');

        // Checking cookie_httponly setting
        if ($this->config['cookie_httponly'] === true) {
            ini_set('session.cookie_httponly', 1);
        }

        // Checking use_only_cookies setting
        if ($this->config['use_only_cookies'] === true) {
            ini_set('session.use_only_cookies', 1);
        }

        // Setting max. lifetime
        ini_set('session.gc_maxlifetime', $this->config['lifetime']);
        session_set_cookie_params($this->config['lifetime']);

        // Initializing
        $this->init();
    }

    /**
     * Initialize session
     *
     * @return void
     */
    private function init()
    {
        if (!isset($_SESSION)) {
            session_start();
            $this->set('session_hash', $this->generateHash());
        } else {
            if ($this->get('session_hash') != $this->generateHash()) {
                $this->destroy();
            }
        }
    }

    /**
     * Set session variable
     *
     * @param string|array $storage
     * @param mixed $content
     * @return void
     */
    public function set($storage, $content = null)
    {
        if (is_array($storage)) {
            foreach ($storage as $key => $value) {
                $_SESSION[$key] = $value;
            }
        } else {
            $_SESSION[$storage] = $content;
        }
    }

    /**
     * Get session variable
     *
     * @param string|null $storage
     * @return mixed
     */
    public function get(string $storage = null)
    {
        return is_null($storage) ? $_SESSION : $_SESSION[$storage];
    }

    /**
     * Check if session variable is exist
     *
     * @param string $storage
     * @return bool
     */
    public function has(string $storage) : bool
    {
        return isset($_SESSION[$storage]);
    }

    /**
     * Delete session
     *
     * @param string|null $storage
     * @return void
     */
    public function delete(string $storage = null)
    {
        if (is_null($storage)) {
            session_unset();
        } else {
            unset($_SESSION[$storage]);
        }
    }

    /**
     * Session destroy
     *
     * @return void
     */
    public function destroy()
    {
        session_destroy();
    }

    /**
     * Set flash message
     *
     * @param string $message
     * @param string|null $url
     * @return void
     */
    public function setFlash(string $message, string $url = null)
    {
        $this->set('flash', $message);

        if (null !== $url) {
            header("Location: $url");
            exit();
        }
    }

    /**
     * Get flash message
     *
     * @return string
     */
    public function getFlash() : string
    {
        $flash = $this->get('flash');
        $this->delete('flash');

        return $flash;
    }

    /**
     * Check if the flash message exists
     *
     * @return bool
     */
    public function hasFlash() : bool
    {
        return $this->has('flash');
    }

    /**
     * Generate Hash for Hijacking Security
     *
     * @return string
     */
    private function generateHash() : string
    {
        if (array_key_exists('REMOTE_ADDR', $_SERVER) && array_key_exists('HTTP_USER_AGENT', $_SERVER)) {
            return md5(sha1(md5($_SERVER['REMOTE_ADDR'] . $this->config['encryption_key'] . $_SERVER['HTTP_USER_AGENT'])));
        }

        return md5(sha1(md5($this->config['encryption_key'])));
    }

}