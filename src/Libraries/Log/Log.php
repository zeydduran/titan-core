<?php

namespace Titan\Libraries\Log;

use Titan\Container\Container;
use Titan\Exception\ExceptionHandler;

class Log
{
    /**
     * Application container
     *
     * @var Container
     */
    private $container;

    /**
     * Log constructor.
     *
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    /**
     * Save log as emergency level
     *
     * @param string $message
     * @throws ExceptionHandler
     */
    public function emergency(string $message)
    {
        $this->write('emergency', $message);
    }

    /**
     * Save log as alert level
     *
     * @param string $message
     * @throws ExceptionHandler
     */
    public function alert(string $message)
    {
        $this->write('alert', $message);
    }

    /**
     * Save log as critical level
     *
     * @param string $message
     * @throws ExceptionHandler
     */
    public function critical(string $message)
    {
        $this->write('critical', $message);
    }

    /**
     * Save log as error level
     *
     * @param string $message
     * @throws ExceptionHandler
     */
    public function error(string $message)
    {
        $this->write('error', $message);
    }

    /**
     * Save log as warning level
     *
     * @param string $message
     * @throws ExceptionHandler
     */
    public function warning(string $message)
    {
        $this->write('warning', $message);
    }

    /**
     * Save log as notice level
     *
     * @param string $message
     * @throws ExceptionHandler
     */
    public function notice(string $message)
    {
        $this->write('notice', $message);
    }

    /**
     * Save log as info level
     *
     * @param string $message
     * @throws ExceptionHandler
     */
    public function info(string $message)
    {
        $this->write('info', $message);
    }

    /**
     * Save log as debug level
     *
     * @param string $message
     * @throws ExceptionHandler
     */
    public function debug(string $message)
    {
        $this->write('debug', $message);
    }

    /**
     * Write log text to the file
     *
     * @param string $level
     * @param string|array $message
     * @throws ExceptionHandler
     */
    protected function write(string $level, $message)
    {
        if (is_array($message)) {
            $message = json_encode($message);
        }

        $this->save('[' . date('Y-m-d H:i:s') . '] - [' . $level . '] ----> ' . $message);
    }

    /**
     * Save log file
     *
     * @param string $text
     * @throws ExceptionHandler
     */
    protected function save(string $text)
    {
        $fileName 	= 'log_' . date('Y-m-d') . '.log';
        $logPath    = $this->container->get('storage_path') . DS . 'Logs';
        $file 		= fopen($logPath . DS . $fileName, 'a');

        if (fwrite($file, $text . "\n") === false) {
            throw new ExceptionHandler("Error", "Log file could not be created. Please check your folder permissions.");
        }

        fclose($file);
    }
}