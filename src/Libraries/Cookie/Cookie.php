<?php

namespace Titan\Libraries\Cookie;

use Titan\Libraries\Config\Config;
use Titan\Exception\ExceptionHandler;

class Cookie
{
    /**
     * Security separator
     *
     * @var string
     */
    private $separator;

    /**
     * Cookie path
     *
     * @var string
     */
    private $path;

    /**
     * Cookie domain
     *
     * @var string
     */
    private $domain;

    /**
     * Cookie secure (https)
     *
     * @var bool
     */
    private $secure;

    /**
     * Cookie http only
     *
     * @var bool
     */
    private $httpOnly;

    /**
     * Cookie config items
     *
     * @var string
     */
    private $config;

    /**
     * Cookie constructor.
     *
     * @param Config $config
     * @throws ExceptionHandler
     * @throws \ReflectionException
     */
    public function __construct(Config $config)
    {
        // Getting cookie config items
        $this->config		= $config->load('app')->get('cookie');

        // Initializing settings
        $this->separator 	= $this->config['separator'];
        $this->path 		= $this->config['path'];
        $this->domain 		= $this->config['domain'];
        $this->secure 		= $this->config['secure'];
        $this->httpOnly		= $this->config['http_only'];
    }

    /**
     * Set cookie path
     *
     * @param string $path
     * @return void
     */
    public function path(string $path)
    {
        $this->path = $path;
    }

    /**
     * Set domain
     *
     * @param string $domain
     * @return void
     */
    public function domain(string $domain)
    {
        $this->domain = $domain;
    }

    /**
     * Set secure (https)
     *
     * @param bool $secure
     * @return void
     */
    public function secure(bool $secure)
    {

        $this->secure = $secure;
    }

    /**
     * Set http only
     *
     * @param bool $http
     * @return void
     */
    public function httpOnly(bool $http)
    {
        $this->httpOnly = $http;
    }

    /**
     * Set cookie
     *
     * @param string $name
     * @param string $value
     * @param integer $time
     * @return void
     */
    public function set(string $name, string $value, int $time = 0)
    {
        if ($time > 0) {
            $time = time() + ($time*60*60);
        }

        if ($this->config['cookie_security'] === true) {
            setcookie(
                $name,
                $value . $this->separator . md5($value . $this->config['encryption_key']),
                $time,
                $this->path,
                $this->domain,
                $this->secure,
                $this->httpOnly
            );
        } else {
            setcookie($name, $value, $time, $this->path, $this->domain, $this->secure, $this->httpOnly);
        }
    }

    /**
     * Get cookie value
     *
     * @param string $name
     * @return mixed
     * @throws ExceptionHandler
     */
    public function get(string $name)
    {
        if ($this->has($name)) {
            if ($this->config['cookie_security'] === true) {
                $parts = explode($this->separator, $_COOKIE[$name]);
                if (md5($parts[0] . $this->config['encryption_key']) == $parts[1]) {
                    return $parts[0];
                }

                throw new ExceptionHandler("Error", "The cookie content is changed");
            }

            return $_COOKIE[$name];
        }

        return false;
    }

    /**
     * Delete cookie
     *
     * @param string $name
     * @return bool
     */
    public function delete(string $name)
    {
        if ($this->has($name)) {
            unset($_COOKIE[$name]);
            setcookie($name, '', time() - 3600, $this->path, $this->domain);
            return true;
        }

        return false;
    }

    /**
     * Check if cookie exist
     *
     * @param string $name
     * @return bool
     */
    public function has(string $name)
    {
        return isset($_COOKIE[$name]);
    }
}