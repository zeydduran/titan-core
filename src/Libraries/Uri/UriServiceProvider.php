<?php

namespace Titan\Libraries\Uri;

use Titan\Kernel\ServiceProvider;

class UriServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind('uri', Uri::class);
    }
}