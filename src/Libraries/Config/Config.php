<?php

namespace Titan\Libraries\Config;

use Titan\Container\Container;
use Titan\Exception\ExceptionHandler;

class Config
{

    /**
     * Config file
     *
     * @var string
     */
    private $file;

    /**
     * Application container
     *
     * @var Container
     */
    private $container;

    /**
     * Config constructor.
     *
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    /**
     * Load the config file
     *
     * @param string $file
     * @throws ExceptionHandler
     * @return $this
     */
    public function load(string $file)
    {
        if (empty($file)) {
            throw new ExceptionHandler('Error', 'Please set a config file');
        }

        $filePath = $this->container->get('config_path') . DS . ucfirst($file) . '.php';

        if (!file_exists($filePath)) {
            throw new ExceptionHandler('Error', "Couldn't find config file --> {$file}");
        }

        $this->file = $filePath;

        return $this;
    }

    /**
     * Get config item
     *
     * @param string|null $params
     * @return mixed
     * @throws \ReflectionException
     */
    public function get(string $params = null)
    {
        // Get config file
        $config = $this->container->resolve('load')->file($this->file);

        // If params is null return config
        if (is_null($params)) {
            return $config;
        }

        // Explode items
        $keys = explode('.', $params);

        // Find the item in array
        foreach ($keys as $key) {
            $config = $config[$key];
        }

        // return the item
        return $config;
    }

}
