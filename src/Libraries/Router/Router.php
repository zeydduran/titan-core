<?php

namespace Titan\Libraries\Router;

use Titan\Container\Container;
use Titan\Exception\ExceptionHandler;

class Router
{
    use RouterTrait;

    /**
     * Routes
     *
     * @var array
     */
    private $routes      = [];

    /**
     * Miidlewares
     *
     * @var array
     */
    private $middlewares = [];

    /**
     * Base route
     *
     * @var string
     */
    private $baseRoute   = '/';

    /**
     * Namespace
     *
     * @var string
     */
    private $namespace   = '';

    /**
     * Domain
     *
     * @var string
     */
    private $domain      = '';

    /**
     * IP
     *
     * @var string
     */
    private $ip          = '';

    /**
     * SSL
     *
     * @var bool
     */
    private $ssl         = false;

    /**
     * Nout found callback
     *
     * @var string
     */
    private $notFound    = '';

    /**
     * Groups
     *
     * @var array
     */
    private $groups      = [];

    /**
     * Names
     *
     * @var array
     */
    private $names       = [];

    /**
     * Group counter
     *
     * @var int
     */
    private $groupped    = 0;

    /**
     * Namespaces
     *
     * @var array
     */
    private $namespaces  = [
        'controllers'   => 'App\\Controllers',
        'middlewares'   => 'App\\Middlewares'
    ];

    /**
     * @var
     */
    private $container;

    /**
     * Router constructor.
     *
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    /**
     * Routing Groups
     *
     * @param callable $callback
     */
    public function group(callable $callback)
    {
        $this->groupped++;

        $this->groups[] = [
            'baseRoute'     => $this->baseRoute,
            'middlewares'   => $this->middlewares,
            'namespace'     => $this->namespace,
            'domain'        => $this->domain,
            'ip'            => $this->ip,
            'ssl'           => $this->ssl
        ];

        // Call the Callable
        call_user_func($callback);

        if ($this->groupped > 0) {
            $this->baseRoute    = $this->groups[$this->groupped-1]['baseRoute'];
            $this->middlewares  = $this->groups[$this->groupped-1]['middlewares'];
            $this->namespace    = $this->groups[$this->groupped-1]['namespace'];
            $this->domain       = $this->groups[$this->groupped-1]['domain'];
            $this->ip           = $this->groups[$this->groupped-1]['ip'];
            $this->ssl          = $this->groups[$this->groupped-1]['ssl'];
        }

        $this->groupped--;

        if ($this->groupped <= 0) {
            // Reset Base Route
            $this->baseRoute    = '/';

            // Reset Middlewares
            $this->middlewares  = [];

            // Reset Namespace
            $this->namespace    = '';

            // Reset Domain
            $this->domain       = '';

            // Reset IP
            $this->ip           = '';

            // Reset SSL
            $this->ssl          = false;
        }
    }

    /**
     * Defining namespace
     *
     * @param string $namespace
     * @return $this
     */
    public function setNamespace(string $namespace)
    {
        // Set Namespace
        $this->namespace = $namespace;

        return $this;
    }

    /**
     * Defining middlewares
     *
     * @param array $middlewares
     * @return $this
     */
    public function middleware(array $middlewares)
    {
        foreach ($middlewares as $middleware) {
            $this->middlewares[$middleware] = [
                'callback' => $this->namespaces['middlewares'] . '\\' . ucfirst($middleware) . '@handle'
            ];
        }

        return $this;
    }

    /**
     * Defining prefix
     *
     * @param string $prefix
     * @return $this
     */
    public function prefix(string $prefix)
    {
        // Set Base Route
        $this->baseRoute    = '/' . $prefix;

        return $this;
    }

    /**
     * Defining domain
     *
     * @param string $domain
     * @return $this
     */
    public function domain(string $domain)
    {
        $this->domain = $domain;
        return $this;
    }

    /**
     * Defining ip address
     *
     * @param string $ip
     * @return $this
     */
    public function ip(string $ip)
    {
        $this->ip = $ip;
        return $this;
    }

    /**
     * Defining ssl
     *
     * @return $this
     */
    public function ssl()
    {
        $this->ssl = true;
        return $this;
    }

    /**
     * @param string $method
     * @param string $uri
     * @param callable $callback
     * @return void
     */
    public function route(string $method, string $uri, $callback)
    {
        // Set Pattern
        $pattern = $this->setPattern($uri, $this->baseRoute);

        // Set Closure
        if (is_callable($callback)) {
            $closure = $callback;
        } elseif (stripos($callback, '@') !== false) {
            if (!empty($this->namespace)) {
                $closure = $this->namespaces['controllers'] . '\\' . ucfirst($this->namespace) . '\\' . $callback;
            } else {
                $closure = $this->namespaces['controllers'] . '\\' . $callback;
            }
        }

        $routeArray = [
            'uri'       => $uri,
            'method'    => $method,
            'pattern'   => $pattern,
            'callback'  => $closure
        ];

        if (!empty($this->namespace)) {
            $routeArray['namespace']    = ucfirst($this->namespace);
        }

        if (!empty($this->middlewares)) {
            $routeArray['middlewares']  = $this->middlewares;
        }

        if (!empty($this->domain)) {
            $routeArray['domain']       = $this->domain;
        }

        if (!empty($this->ip)) {
            $routeArray['ip']           = $this->ip;
        }

        if ($this->ssl) {
            $routeArray['ssl']          = $this->ssl;
        }

        $this->routes[] = $routeArray;
    }

    /**
     * Add a route using GET method
     *
     * @param string $pattern
     * @param string|callable $callback
     * @return $this
     */
    public function get(string $pattern, $callback)
    {
        $this->route('GET', $pattern, $callback);
        return $this;
    }

    /**
     * Add a route using POST method
     *
     * @param string $pattern
     * @param string|callable $callback
     * @return $this
     */
    public function post(string $pattern, $callback)
    {
        $this->route('POST', $pattern, $callback);
        return $this;
    }

    /**
     * Add a route using PATCH method
     *
     * @param string $pattern
     * @param string|callable $callback
     * @return $this
     */
    public function patch(string $pattern, $callback)
    {
        $this->route('PATCH', $pattern, $callback);
        return $this;
    }

    /**
     * Add a route using DELETE method
     *
     * @param string $pattern
     * @param string|callable $callback
     * @return $this
     */
    public function delete(string $pattern, $callback)
    {
        $this->route('DELETE', $pattern, $callback);
        return $this;
    }

    /**
     * Add a route using PUT method
     *
     * @param string $pattern
     * @param string|callable $callback
     * @return $this
     */
    public function put(string $pattern, $callback)
    {
        $this->route('PUT', $pattern, $callback);
        return $this;
    }

    /**
     * Add a route using OPTIONS method
     *
     * @param string $pattern
     * @param string|callable $callback
     * @return $this
     */
    public function options(string $pattern, $callback)
    {
        $this->route('OPTIONS', $pattern, $callback);
        return $this;
    }

    /**
     * @param array $methods
     * @param string $pattern
     * @param string|callable $callback
     * @return $this
     */
    public function match(array $methods, string $pattern, $callback)
    {
        foreach ($methods as $method) {
            $this->route(strtoupper($method), $pattern, $callback);
        }

        return $this;
    }

    /**
     * Set regular expression for parameters in the querystring
     *
     * @param $expressions
     * @return $this
     */
    public function where($expressions)
    {
        $routeKey   = array_search(end($this->routes), $this->routes);
        $pattern    = $this->parseUri($this->routes[$routeKey]['uri'], $expressions);
        $pattern    = '/' . implode('/', $pattern);
        $pattern    = '/^' . str_replace('/', '\/', $pattern) . '$/';

        $this->routes[$routeKey]['pattern'] = $pattern;

        return $this;
    }

    /**
     * Set name for a route
     *
     * @param string $name
     * @return $this
     */
    public function name(string $name)
    {
        $routeKey = array_search(end($this->routes), $this->routes);
        $this->routes[$routeKey]['name'] = $name;

        return $this;
    }

    /**
     * Get all routes
     *
     * @return array
     */
    public function getRoutes() : array
    {
        return $this->routes;
    }

    /**
     * Set the 404 handling function
     *
     * @param $callback
     */
    public function set404($callback)
    {
        $this->notFound = $callback;
    }

    /**
     * Run routing
     *
     * @throws ExceptionHandler
     * @throws \ReflectionException
     */
    public function run()
    {
        $matched = 0;

        foreach ($this->routes as $key => $val) {
            if (preg_match($val['pattern'], $this->getCurrentUri(), $params)) {

                // Checking domain
                $domainCheck    = $this->checkDomain($val);

                // Checking IP
                $ipCheck        = $this->checkIp($val);

                // Checking SSL
                $sslCheck       = $this->checkSSL($val);

                // Checking request method
                $methodCheck    = $this->checkMethod($val);

                if ($domainCheck && $methodCheck && $ipCheck && $sslCheck) {
                    $matched++;

                    array_shift($params);

                    // Checking middlewares
                    if (array_key_exists('middlewares', $val)) {
                        foreach ($val['middlewares'] as $midKey => $midVal) {
                            list($controller, $method) = explode('@', $midVal['callback']);

                            if (class_exists($controller)) {

                                $action     = new \ReflectionMethod($controller, $method);
                                $parameters = $action->getParameters();
                                $args       = [];

                                foreach ($parameters as $parameter) {
                                    if (isset($parameter->getClass()->name)) {
                                        $class  = $parameter->getClass()->name;
                                        $args[] = $this->container->resolve($class);
                                    }
                                }

                                $action->invokeArgs(new $controller, $args);
                            }
                        }
                    }

                    if (is_callable($val['callback'])) {
                        call_user_func_array($val['callback'], array_values($params));
                    } else if (stripos($val['callback'], '@') !== false) {
                        list($controller, $method) = explode('@', $val['callback']);

                        if (class_exists($controller)) {
                            $action     = new \ReflectionMethod($controller, $method);
                            $parameters = $action->getParameters();
                            $args       = [];
                            $index      = 0;

                            foreach ($parameters as $parameter) {
                                if (isset($parameter->getClass()->name)) {
                                    $class  = $parameter->getClass()->name;
                                    $args[] = $this->container->resolve($class);
                                } else {
                                    $args[] = $params[$index];
                                    $index++;
                                }
                            }

                            $action->invokeArgs(new $controller, $args);
                        } else {
                            $this->pageNotFound();
                        }
                    }

                    break;
                }

            }
        }

        if ($matched === 0)
            $this->pageNotFound();
    }

    /**
     * Page not found redirection
     *
     * @throws ExceptionHandler
     */
    private function pageNotFound()
    {
        if (!empty($this->notFound) && is_callable($this->notFound)) {
            call_user_func($this->notFound);
        } else {
            header($_SERVER['SERVER_PROTOCOL'] . ' 404 Not Found');
            throw new ExceptionHandler("Hata", "Controller bulunamadı");
        }
    }

    /**
     * Get url based on named route
     *
     * @param string $name
     * @param array $params
     * @param int $type
     * @return array|string
     */
    private function getUrl(string $name, array $params = [], int $type = 0)
    {
        if (!empty($this->routes)) {
            foreach ($this->routes as $route) {
                if (array_key_exists('name', $route) && $route['name'] == $name) {
                    $uri = $route['uri'];
                    $pattern = $this->parseUri($uri, $params);
                    $pattern = implode('/', $pattern);
                    break;
                }
            }

            if ($type === 0) {
                return empty($pattern) ? '/' : $pattern;
            }

            return empty($pattern) ? $this->generateUrl() : $this->generateUrl($pattern);
        }

        return false;
    }

    /**
     * @param string $method
     * @param array $args
     * @return $this
     */
    public function __call(string $method, array $args = [])
    {
        if ($method == 'namespace') {
            $this->setNamespace($args[0]);
            return $this;
        }
    }

}