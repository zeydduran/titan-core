<?php

namespace Titan\Libraries\Router;

use Titan\Kernel\ServiceProvider;

class RouterServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton('router', Router::class);
    }
}