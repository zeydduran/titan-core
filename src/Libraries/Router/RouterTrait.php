<?php

namespace Titan\Libraries\Router;

trait RouterTrait
{
    /**
     * Set the url pattern
     *
     * @param string $pattern
     * @param string $baseRoute
     * @return null|string|string[]
     */
    protected function setPattern(string $pattern, string $baseRoute)
    {
        if ($pattern == '/') {
            $pattern = $baseRoute . trim($pattern, '/');
        } else {
            if ($baseRoute == '/') {
                $pattern = $baseRoute . trim($pattern, '/');
            } else {
                $pattern = $baseRoute . $pattern;
            }
        }

        $pattern = preg_replace('/[\[{\(].*[\]}\)]/U', '([^/]+)', $pattern);
        $pattern = '/^' . str_replace('/', '\/', $pattern) . '$/';

        return $pattern;
    }

    /**
     * Check domain
     *
     * @param array $params
     * @return bool
     */
    protected function checkDomain(array $params) : bool
    {
        if (array_key_exists('domain', $params)) {
            if ($params['domain'] !== trim(str_replace('www.', '', $_SERVER['SERVER_NAME']), '/')) {
                return false;
            }

            return true;
        }

        return true;
    }

    /**
     * Check request method
     *
     * @param array $params
     * @return bool
     */
    protected function checkMethod(array $params) : bool
    {
        if ($params['method'] !== $this->getRequestMethod()) {
            return false;
        }

        return true;
    }

    /**
     * Check IP address
     *
     * @param array $params
     * @return bool
     */
    protected function checkIp(array $params) : bool
    {
        if (array_key_exists('ip', $params)) {
            if (is_array($params['ip'])) {
                if (!in_array($_SERVER['REMOTE_ADDR'], $params['ip'])) {
                    return false;
                }

                return true;
            } else {
                if ($_SERVER['REMOTE_ADDR'] != $params['ip']) {
                    return false;
                }

                return true;
            }
        }

        return true;
    }

    /**
     * Check ssl
     *
     * @param array $params
     * @return bool
     */
    protected function checkSSL(array $params) : bool
    {
        if (array_key_exists('ssl', $params) && $params['ssl'] === true) {
            if ($_SERVER['REQUEST_SCHEME'] !== 'https') {
                return false;
            }

            return true;
        }

        return true;
    }

    /**
     * Get request header
     *
     * @return array|false
     */
    protected function getRequestHeaders()
    {
        // If getallheaders() is available, use that
        if (function_exists('getallheaders')) {
            return getallheaders();
        }

        // If getallheaders() is not available, use that
        $headers = [];
        foreach ($_SERVER as $name => $value) {
            if ((substr($name, 0, 5) == 'HTTP_') || ($name == 'CONTENT_TYPE') || ($name == 'CONTENT_LENGTH')) {
                $headers[str_replace(array(' ', 'Http'), array('-', 'HTTP'), ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
            }
        }

        return $headers;
    }

    /**
     * Get request method
     *
     * @return mixed|string
     */
    protected function getRequestMethod()
    {
        // Take the method as found in $_SERVER
        $method = $_SERVER['REQUEST_METHOD'];

        // If it's a HEAD request override it to being GET and prevent any output, as per HTTP Specification
        if ($_SERVER['REQUEST_METHOD'] == 'HEAD') {
            ob_start();
            $method = 'GET';
        }

        // If it's a POST request, check for a method override header
        elseif ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $headers = $this->getRequestHeaders();
            if (isset($headers['X-HTTP-Method-Override']) && in_array($headers['X-HTTP-Method-Override'], array('PUT', 'DELETE', 'PATCH'))) {
                $method = $headers['X-HTTP-Method-Override'];
            }
        }

        return $method;
    }

    /**
     * Parse url with parameters
     *
     * @param string $uri
     * @param array $expressions
     * @return array
     */
    protected function parseUri(string $uri, array $expressions = []) : array
    {
        $pattern = explode('/', ltrim($uri, '/'));
        foreach ($pattern as $key => $val) {
            if(preg_match('/[\[{\(].*[\]}\)]/U', $val, $matches)) {
                foreach ($matches as $match) {
                    $matchKey = substr($match, 1, -1);
                    if (array_key_exists($matchKey, $expressions)) {
                        $pattern[$key] = $expressions[$matchKey];
                    }
                }
            }
        }

        return $pattern;
    }

    /**
     * Get base path
     *
     * @return string
     */
    protected function getBasePath() : string
    {
        $scriptName = array_slice(explode('/', $_SERVER['SCRIPT_NAME']), 0, -1);
        return implode('/', $scriptName) . '/';
    }

    /**
     * Get current uri
     *
     * @return string
     */
    protected function getCurrentUri() : string
    {
        // Get the current Request URI and remove rewrite base path from it
        $uri = substr($_SERVER['REQUEST_URI'], strlen($this->getBasePath()));

        // Don't take query params into account on the URL
        if (strstr($uri, '?')) {
            $uri = substr($uri, 0, strpos($uri, '?'));
        }

        // Remove trailing slash + enforce a slash at the start
        return '/' . trim($uri, '/');
    }

    /**
     * Generates an absolute url
     *
     * @param string|null $url
     * @return string
     */
    protected function generateUrl(string $url = null) : string
    {
        if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off') {
            $protocol = 'https';
        } else {
            $protocol = 'http';
        }

        if (is_null($url)) {
            return $protocol . "://" . $_SERVER['HTTP_HOST'];
        }

        return $protocol . "://" . rtrim($_SERVER['HTTP_HOST'], '/') . '/' . $url;
    }
}