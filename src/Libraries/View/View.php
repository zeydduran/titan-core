<?php

namespace Titan\Libraries\View;

use Titan\Container\Container;
use Titan\Exception\ExceptionHandler;
use Windwalker\Edge\Cache\EdgeFileCache;
use Windwalker\Edge\Edge;
use Windwalker\Edge\Loader\EdgeFileLoader;

class View
{
    /**
     * Container instance
     *
     * @var Container
     */
    private $container;

    /**
     * Selected theme
     *
     * @var string|null
     */
    private $theme = null;

    /**
     * View constructor.
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    /**
     * Render view file
     *
     * @param string $file
     * @param array $vars
     * @param bool $cache
     * @return void
     */
    public function render(string $file, array $vars = [], $cache = true)
    {
        $paths  = [
            $this->container->get('view_path')
        ];

        $loader = new EdgeFileLoader($paths);
        $loader->addFileExtension('.blade.php');

        if ($cache) {
            $cachePath  = $this->container->get('storage_path') . DS . 'Cache';
            $edge       = new Edge(new EdgeFileLoader($paths), null, new EdgeFileCache($cachePath));
        } else {
            $edge       = new Edge($loader);
        }

        if (is_null($this->theme)) {
            echo $edge->render($file, $vars);
        } else {
            echo $edge->render($this->theme . $file, $vars);
        }
    }

    /**
     * Set active theme
     *
     * @param string $theme
     * @return $this
     * @throws ExceptionHandler
     */
    public function theme(string $theme)
    {
        if (file_exists($this->container->get('view_path') . DS . $theme)) {
            $this->theme = $theme;
        } else {
            throw new ExceptionHandler('Error', 'Template directory is not found : { '. $theme .' }');
        }

        return $this;
    }
    
}