<?php

namespace Titan\Libraries\Cache;

use Titan\Kernel\ServiceProvider;

class CacheServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind('cache', Cache::class);
    }
}