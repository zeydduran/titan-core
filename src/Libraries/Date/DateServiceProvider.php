<?php

namespace Titan\Libraries\Date;

use Titan\Kernel\ServiceProvider;

class DateServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind('date', Date::class);
    }
}