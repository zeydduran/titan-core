<?php

namespace Titan\Libraries\Http\Request;

use Titan\Kernel\ServiceProvider;

class RequestServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind('request', Request::class);
    }
}