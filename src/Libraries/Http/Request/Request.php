<?php

namespace Titan\Libraries\Http\Request;

class Request
{
    /**
     * Get variables
     *
     * @var array
     */
    protected $getVars;

    /**
     * Post variables
     *
     * @var array
     */
    protected $postVars;

    /**
     * Cookie variables
     *
     * @var array
     */
    protected $cookieVars;

    /**
     * File variables
     *
     * @var array
     */
    protected $filesVars;

    /**
     * Server variables
     *
     * @var array
     */
    protected $serverVars;

    /**
     * Global variables
     *
     * @var array
     */
    protected $globalVars;

    /**
     * Request constructor.
     */
    public function __construct()
    {
        $this->createGlobals();
    }

    /**
     * Create global variables
     *
     * @return void
     */
    private function createGlobals()
    {
        foreach ($GLOBALS as $key => $value) {
            switch ($key) {
                case '_GET':
                    $this->getVars = $value;
                    break;
                case '_POST':
                    $this->postVars = $value;
                    break;
                case '_COOKIE' :
                    $this->cookieVars = $value;
                    break;
                case '_FILES' :
                    $this->filesVars = $value;
                    break;
                case '_SERVER' :
                    $this->serverVars = $value;
                    break;
                case 'GLOBALS' :
                    $this->globalVars = $value;
                    break;
            }
        }
    }

    /**
     * Get Server Variables
     *
     * @param string|array|null $param
     * @return mixed
     */
    public function server($param = null)
    {
        if (is_null($param)) {
            return $this->serverVars;
        } else if (is_array($param)) {
            return $this->getBulkData($param, $this->serverVars, false);
        }

        return $this->serverVars[$param] ?? null;
    }

    /**
     * Get HTTP Headers
     *
     * @param string|array|null $param
     * @return mixed
     */
    public function header($param = null)
    {
        $headers = $this->getAllHeaders();

        if (is_null($param)) {
            return $headers;
        } else if (is_array($param)) {
            return $this->getBulkData($param, $headers, false);
        }

        return $headers[$param] ?? null;
    }

    /**
     * Get All Inputs
     *
     * @param bool $filter
     * @return array
     */
    public function all(bool $filter = true) : array
    {
        return $this->filter($_REQUEST, $filter);
    }

    /**
     * Get Variables
     *
     * @param string|array|null $param
     * @param bool $filter
     * @return mixed
     */
    public function get($param = null, bool $filter = true)
    {
        if (is_null($param)) {
            return $this->filter($this->getVars, $filter);
        } else if (is_array($param)) {
            return $this->getBulkData($param, $this->getVars, $filter);
        }

        return $this->filter($this->getVars[$param], $filter) ?? null;
    }

    /**
     * Post Variables
     *
     * @param string|array|null $param
     * @param bool $filter
     * @return mixed
     */
    public function post($param = null, bool $filter = true)
    {
        if (is_null($param)) {
            return $this->filter($this->postVars, $filter);
        } else if (is_array($param)) {
            return $this->getBulkData($param, $this->postVars, $filter);
        }

        return $this->filter($this->postVars[$param], $filter) ?? null;
    }

    /**
     * Put Variables
     *
     * @param string|array|null $param
     * @param bool $filter
     * @return mixed
     */
    public function put($param = null, bool $filter = true)
    {
        parse_str(file_get_contents("php://input"), $_PUT);

        if (is_null($param)) {
            return $this->filter($_PUT, $filter);
        } else if (is_array($param)) {
            return $this->getBulkData($param, $_PUT, $filter);
        }

        return $this->filter($_PUT[$param], $filter) ?? null;
    }

    /**
     * Patch Variables
     *
     * @param string|array|null $param
     * @param bool $filter
     * @return mixed
     */
    public function patch($param = null, bool $filter = true)
    {
        parse_str(file_get_contents('php://input'), $_PATCH);

        if (is_null($param)) {
            return $this->filter($_PATCH, $filter);
        } else if (is_array($param)) {
            return $this->getBulkData($param, $_PATCH, $filter);
        }

        return $this->filter($_PATCH[$param], $filter) ?? null;
    }

    /**
     * Delete Variables
     *
     * @param string|array|null $param
     * @param bool $filter
     * @return mixed
     */
    public function delete($param = null, bool $filter = true)
    {
        parse_str(file_get_contents("php://input"), $_DELETE);

        if (is_null($param)) {
            return $this->filter($_DELETE, $filter);
        } else if (is_array($param)) {
            return $this->getBulkData($param, $_DELETE, $filter);
        }

        return $this->filter($_DELETE[$param], $filter) ?? null;
    }

    /**
     * Get Cookie Variables
     *
     * @param string|array|null $param
     * @return mixed
     */
    public function cookie($param = null)
    {
        if (is_null($param)) {
            return $this->cookieVars;
        } else if (is_array($param)) {
            return $this->getBulkData($param, $this->cookieVars, false);
        }

        return $this->cookieVars[$param] ?? null;
    }

    /**
     * Get File Variables
     *
     * @param string|array|null $param
     * @return array
     */
    public function files($param = null) : array
    {
        if (is_null($param)) {
            return $this->filesVars;
        } else if (is_array($param)) {
            return $this->getBulkData($param, $this->filesVars, false);
        }

        return $this->filesVars[$param] ?? null;
    }

    /**
     * Get Globals
     *
     * @param string|array|null $param
     * @return mixed
     */
    public function globals($param = null)
    {
        if (is_null($param)) {
            return $this->globalVars;
        } else if (is_array($param)) {
            return $this->getBulkData($param, $this->globalVars, false);
        }

        return $this->globalVars[$param] ?? null;
    }

    /**
     * Get Request Method
     *
     * @return string
     */
    public function getRequestMethod() : string
    {
        return $this->server('REQUEST_METHOD');
    }

    /**
     * Get Script Name
     *
     * @return string
     */
    public function getScriptName() : string
    {
        return $this->server('SCRIPT_NAME');
    }

    /**
     * Get Request Scheme
     *
     * @return string
     */
    public function getScheme() : string
    {
        return stripos($this->server('SERVER_PROTOCOL'), 'https') === true ? 'https' : 'http';
    }

    /**
     * Get Http Host
     *
     * @return string
     */
    public function getHost() : string
    {
        return $this->server('HTTP_HOST');
    }

    /**
     * Get Request URI
     *
     * @return string
     */
    public function getRequestUri() : string
    {
        return $this->server('REQUEST_URI');
    }

    /**
     * Get Base URL
     *
     * @param string $url
     * @return string
     */
    public function baseUrl(string $url = null) : string
    {
        if (is_null($url)) {
            return $this->getScheme() . '://' . $this->getHost();
        }

        return $this->getScheme() . '://' . rtrim($this->getHost(), '/') . '/' . $url;
    }

    /**
     * Get URL Segments
     *
     * @return array
     */
    public function segments() : array
    {
        return explode('/', trim(parse_url($this->getRequestUri(), PHP_URL_PATH), '/'));
    }

    /**
     * Get specified segment from URL
     *
     * @param int $index
     * @return string
     */
    public function getSegment(int $index = 0) : string
    {
        return $this->segments()[$index];
    }

    /**
     * Get current URL Segment
     *
     * @return string
     */
    public function currentSegment() : string
    {
        return $this->getSegment(count($this->segments()) - 1);
    }

    /**
     * Get Query String Elements
     *
     * @param bool $array (If true then return as an array)
     * @return string|array
     */
    public function getQueryString(bool $array = false)
    {
        if ($array === false) {
            return $this->server('QUERY_STRING');
        } else {
            $qsParts	= explode('&', $this->server('QUERY_STRING'));
            $qsArray 	= [];

            foreach ($qsParts as $key => $value) {
                $qsItems 				= explode('=', $value);
                $qsArray[$qsItems[0]] 	= $qsItems[1];
            }

            return $qsArray;
        }
    }

    /**
     * Get Content Type
     *
     * @return string
     */
    public function getContentType() : string
    {
        return explode(',', $this->header()['Accept'])[0];
    }

    /**
     * Get Locales
     *
     * @return array
     */
    public function getLocales() : array
    {
        return explode(',', preg_replace('/(;q=[0-9\.]+)/i', '', strtolower(trim($this->server('HTTP_ACCEPT_LANGUAGE')))));
    }

    /**
     * Get the locale
     *
     * @return string
     */
    public function getLocale() : string
    {
        return $this->getLocales()[0];
    }

    /**
     * Check if the requested method is of specified type
     *
     * @param string $method
     * @return bool
     */
    public function isMethod(string $method) : bool
    {
        return $this->getRequestMethod() === strtoupper($method);
    }


    /**
     * Check if the request is an ajax request
     *
     * @return bool
     */
    public function isAjax() : bool
    {
        if (null !== $this->server('HTTP_X_REQUESTED_WITH') && strtolower($this->server('HTTP_X_REQUESTED_WITH')) === 'xmlhttprequest') {
            return true;
        }

        return false;
    }

    /**
     * Check if the http request is secure
     *
     * @return bool
     */
    public function isSecure() : bool
    {
        if (
            null !== $this->server('HTTPS') || (null !== $this->server('HTTP_X_FORWARDED_PROTO') && $this->server('HTTP_X_FORWARDED_PROTO') == 'https')
        ) {
            return true;
        }

        return false;
    }

    /**
     * Check if the visitor is robot
     *
     * @return bool
     */
    public function isRobot() : bool
    {
        if (null !== $this->server('HTTP_USER_AGENT') && preg_match('/bot|crawl|slurp|spider/i', $this->server('HTTP_USER_AGENT'))) {
            return true;
        }

        return false;
    }

    /**
     * Check if the visitor is mobile
     *
     * @return bool
     */
    public function isMobile() : bool
    {
        return preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $this->server("HTTP_USER_AGENT"));
    }

    /**
     * Check is referral
     *
     * @return bool
     */
    public function isReferral() : bool
    {
        if (null !== $this->server('HTTP_REFERER') || $this->server('HTTP_REFERER') == '') {
            return false;
        }

        return true;
    }

    /**
     * Return Http Referrer
     *
     * @return string
     */
    public function getReferrer() : string
    {
        return ($this->isReferral()) ? trim($this->server('HTTP_REFERER')) : '';
    }

    /**
     * Get client IP
     *
     * @return string
     */
    public function getIp() : string
    {
        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if (getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if (getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if (getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if (getenv('HTTP_FORWARDED'))
            $ipaddress = getenv('HTTP_FORWARDED');
        else if (getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else
            $ipaddress = 'UNKNOWN';

        return $ipaddress;
    }

    /**
     * Filter inputs
     *
     * @param string|null $data
     * @param bool $filter
     * @return mixed
     */
    public function filter(string $data = null, bool $filter = false)
    {
        if (is_null($data)) {
            return null;
        }

        if (is_array($data)) {
            return $filter === true ? array_map([$this, 'xssClean'], $data) : array_map('trim', $data);
        }

        return $filter === true ? $this->xssClean($data) : trim($data);
    }

    /**
     * Clear XSS
     *
     * @param string $data
     * @return string
     */
    public function xssClean(string $data) : string
    {
        // Fix &entity\n;
        $data = str_replace(array('&amp;','&lt;','&gt;'), array('&amp;amp;','&amp;lt;','&amp;gt;'), $data);
        $data = preg_replace('/(&#*\w+)[\x00-\x20]+;/u', '$1;', $data);
        $data = preg_replace('/(&#x*[0-9A-F]+);*/iu', '$1;', $data);
        $data = html_entity_decode($data, ENT_COMPAT, 'UTF-8');

        // Remove any attribute starting with "on" or xmlns
        $data = preg_replace('#(<[^>]+?[\x00-\x20"\'])(?:on|xmlns)[^>]*+>#iu', '$1>', $data);

        // Remove javascript: and vbscript: protocols
        $data = preg_replace('#([a-z]*)[\x00-\x20]*=[\x00-\x20]*([`\'"]*)[\x00-\x20]*j[\x00-\x20]*a[\x00-\x20]*v[\x00-\x20]*a[\x00-\x20]*s[\x00-\x20]*c[\x00-\x20]*r[\x00-\x20]*i[\x00-\x20]*p[\x00-\x20]*t[\x00-\x20]*:#iu', '$1=$2nojavascript...', $data);
        $data = preg_replace('#([a-z]*)[\x00-\x20]*=([\'"]*)[\x00-\x20]*v[\x00-\x20]*b[\x00-\x20]*s[\x00-\x20]*c[\x00-\x20]*r[\x00-\x20]*i[\x00-\x20]*p[\x00-\x20]*t[\x00-\x20]*:#iu', '$1=$2novbscript...', $data);
        $data = preg_replace('#([a-z]*)[\x00-\x20]*=([\'"]*)[\x00-\x20]*-moz-binding[\x00-\x20]*:#u', '$1=$2nomozbinding...', $data);

        // Only works in IE: <span style="width: expression(alert('Ping!'));"></span>
        $data = preg_replace('#(<[^>]+?)style[\x00-\x20]*=[\x00-\x20]*[`\'"]*.*?expression[\x00-\x20]*\([^>]*+>#i', '$1>', $data);
        $data = preg_replace('#(<[^>]+?)style[\x00-\x20]*=[\x00-\x20]*[`\'"]*.*?behaviour[\x00-\x20]*\([^>]*+>#i', '$1>', $data);
        $data = preg_replace('#(<[^>]+?)style[\x00-\x20]*=[\x00-\x20]*[`\'"]*.*?s[\x00-\x20]*c[\x00-\x20]*r[\x00-\x20]*i[\x00-\x20]*p[\x00-\x20]*t[\x00-\x20]*:*[^>]*+>#iu', '$1>', $data);

        // Remove namespaced elements (we do not need them)
        $data = preg_replace('#</*\w+:\w[^>]*+>#i', '', $data);

        do
        {
            // Remove really unwanted tags
            $old_data = $data;
            $data = preg_replace('#</*(?:applet|b(?:ase|gsound|link)|embed|frame(?:set)?|i(?:frame|layer)|l(?:ayer|ink)|meta|object|s(?:cript|tyle)|title|xml)[^>]*+>#i', '', $data);
        }
        while ($old_data !== $data);

        // we are done...
        return $data;
    }

    /**
     * Clean HTML
     *
     * @param string $data
     * @return string
     */
    public function htmlClean(string $data) : string
    {
        return strip_tags(htmlentities(trim(stripslashes($data)), ENT_NOQUOTES, "UTF-8"));
    }

    /**
     * Get all headers
     *
     * @return array
     */
    private function getAllHeaders() : array
    {
        $headers = [];

        foreach ($_SERVER as $name => $value) {
            if ((substr($name, 0, 5) == 'HTTP_') || ($name == 'CONTENT_TYPE') || ($name == 'CONTENT_LENGTH')) {
                $headers[str_replace(array(' ', 'Http'), array('-', 'HTTP'), ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
            }
        }

        return $headers;
    }

    /**
     * Get bulk data from global variables
     *
     * @param array $params
     * @param array $data
     * @param bool $filter
     * @return array
     */
    private function getBulkData(array $params, array $data, bool $filter = true) : array
    {
        $vars = [];

        foreach ($params as $param) {
            $vars[$param] = $this->filter($data[$param], $filter) ?? null;
        }

        return $vars;
    }
}