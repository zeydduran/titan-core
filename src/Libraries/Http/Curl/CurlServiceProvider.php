<?php

namespace Titan\Libraries\Http\Curl;

use Titan\Kernel\ServiceProvider;

class CurlServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind('curl', Curl::class);
    }
}