<?php

namespace Titan\Libraries\Http\Response;

class Response
{
    /**
     * HTTP Status codes
     *
     * @var array
     */
    private $statusCodes    = [
        100 => 'Continue',
        101 => 'Switching Protocols',
        200 => 'OK',
        201 => 'Created',
        202 => 'Accepted',
        203 => 'Non-Authoritative Information',
        204 => 'No Content',
        205 => 'Reset Content',
        206 => 'Partial Content',
        300 => 'Multiple Choices',
        301 => 'Moved Permanently',
        302 => 'Found',
        303 => 'See Other',
        304 => 'Not Modified',
        305 => 'Use Proxy',
        306 => '(Unused)',
        307 => 'Temporary Redirect',
        400 => 'Bad Request',
        401 => 'Unauthorized',
        402 => 'Payment Required',
        403 => 'Forbidden',
        404 => 'Not Found',
        405 => 'Method Not Allowed',
        406 => 'Not Acceptable',
        407 => 'Proxy Authentication Required',
        408 => 'Request Timeout',
        409 => 'Conflict',
        410 => 'Gone',
        411 => 'Length Required',
        412 => 'Precondition Failed',
        413 => 'Request Entity Too Large',
        414 => 'Request-URI Too Long',
        415 => 'Unsupported Media Type',
        416 => 'Requested Range Not Satisfiable',
        417 => 'Expectation Failed',
        500 => 'Internal Server Error',
        501 => 'Not Implemented',
        502 => 'Bad Gateway',
        503 => 'Service Unavailable',
        504 => 'Gateway Timeout',
        505 => 'HTTP Version Not Supported'
    ];

    /**
     * Content charset
     *
     * @var string
     */
    private $charset        = 'utf-8';

    /**
     * Http status code
     *
     * @var int
     */
    private $statusCode     = 200;

    /**
     * Content type
     *
     * @var string
     */
    private $contentType    = 'text/html';

    /**
     * Http headers
     *
     * @var array
     */
    private $headers        = [];

    /**
     * Set content charste
     *
     * @param string $charset
     * @return $this
     */
    public function setCharset(string $charset)
    {
        $this->charset = $charset;

        return $this;
    }

    /**
     * Get content charset
     *
     * @return string
     */
    public function getCharset() : string
    {
        return $this->charset;
    }

    /**
     * Set http status code
     *
     * @param int $code
     * @return $this
     */
    public function setStatusCode(int $code)
    {
        $this->statusCode = $code;

        return $this;
    }

    /**
     * Get http status code
     *
     * @return int
     */
    public function getStatusCode() : int
    {
        return $this->statusCode;
    }

    /**
     * Get http status message
     *
     * @param int|null $code
     * @return string
     */
    public function getStatusText(int $code = null) : string
    {
        if (is_null($code)) {
            return $this->statusCodes[$this->statusCode];
        }

        return $this->statusCodes[$code];
    }

    /**
     * Set content type
     *
     * @param string $type
     * @return $this
     */
    public function setContentType(string $type)
    {
        $this->contentType = $type;

        return $this;
    }

    /**
     * Get content type
     *
     * @return string
     */
    public function getContentType() : string
    {
        return $this->contentType;
    }

    /**
     * Set http headers
     *
     * @param $headers
     * @return $this
     */
    public function setHeaders(array $headers)
    {
        foreach ($headers as $key => $value) {
            $this->headers[$key] = $value;
        }

        return $this;
    }

    /**
     * Send content
     *
     * @param mixed $content
     * @return void
     */
    public function send($content)
    {
        // Remove all headers
        header_remove();

        // Setting http status code
        http_response_code($this->statusCode);

        // Setting content type and charset
        header('Content-Type: ' . $this->contentType . '; charset=' . $this->charset);

        // Setting headers
        foreach ($this->headers as $key => $value) {
            header($key . ': ' . $value);
        }

        echo $content;
    }

    /**
     * Send json response
     *
     * @param mixed $content
     * @return void
     */
    public function json($content)
    {
        // Remove all headers
        header_remove();

        // Setting http status code
        http_response_code($this->statusCode);

        // Setting content type
        header('Content-type: application/json');

        echo json_encode($content);
    }
}