<?php

namespace Titan\Libraries\Http\Response;

use Titan\Kernel\ServiceProvider;

class ResponseServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind('response', Response::class);
    }
}