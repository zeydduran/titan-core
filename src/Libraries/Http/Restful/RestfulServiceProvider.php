<?php

namespace Titan\Libraries\Http\Restful;

use Titan\Kernel\ServiceProvider;

class RestfulServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind('restful', Restful::class);
    }
}