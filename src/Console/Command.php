<?php

namespace Titan\Console;

class Command
{
    /**
     * @var
     */
    protected $app;

    /**
     * Command list
     *
     * @var array
     */
    protected $commandList = [
        'Titan\Console\Commands\Make\ControllerCommand',
        'Titan\Console\Commands\Make\ModelCommand',
        'Titan\Console\Commands\Make\MiddlewareCommand'
    ];

    /**
     * Command constructor.
     *
     * @param $app
     * @return void
     */
    public function __construct($app)
    {
        $this->app = $app;
        $this->generate();
    }

    /**
     * Run commands
     *
     * @return void
     */
    public function run()
    {
        $this->app->run();
        exit;
    }

    /**
     * Generate all commands
     *
     * @return void
     */
    private function generate()
    {
        foreach ($this->commandList as $command) {
            $this->app->add(new $command);
        }
    }
}