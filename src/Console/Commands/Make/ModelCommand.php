<?php

namespace Titan\Console\Commands\Make;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ModelCommand extends Command
{
    /**
     * Configure the command
     */
    protected function configure()
    {
        $this->setName('make:model')
            ->addArgument('name', InputArgument::REQUIRED, 'The name for the model.')
            ->addOption('--force', '-f', InputOption::VALUE_OPTIONAL, 'Force to re-create model.')
            ->setDescription('Create a new model.')
            ->setHelp("This command makes you to create model...");
    }

    /**
     * Execute the command
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $name = $input->getArgument('name');
        $force = $input->hasParameterOption('--force');
        $file = ROOT . '/App/Models/' . $name . '.php';

        if (!file_exists($file)) {
            $this->createNewFile($file, $name);
            $output->writeln(
                "\n" . ' <info>+Success!</info> "' . ($name) . '" model created.'
            );
        } else {
            if ($force !== false) {
                unlink($file);
                $this->createNewFile($file, $name);
                $output->writeln(
                    "\n" . ' <info>+Success!</info> "' . ($name) . '" model re-created.'
                );
            } else {
                $output->writeln(
                    "\n" . ' <error>-Error!</error> Model already exists! (' . $name . ')'
                );
            }
        }

        return;
    }

    /**
     * Create new model file
     *
     * @param string $file
     * @param string $name
     * @return void
     */
    private function createNewFile($file, $name)
    {
        $model = ucfirst($name);
        $contents = <<<PHP
<?php
namespace App\Models;

use DB;

class $model
{
    // Write your methods
}
PHP;
        if (false === file_put_contents($file, $contents)) {
            throw new \RuntimeException(sprintf('The file "%s" could not be written to', $file));
        }

        return;
    }

}